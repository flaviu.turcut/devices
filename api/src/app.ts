import express from 'express';
import mockDevices from './utils/mockDevices';

const app = express();

app.get('/devices', (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.json(mockDevices);
});

app.get('/devices/status/:status', (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    const { status } = req.params;

    const devices = mockDevices.filter((device) => device.status === status);

    res.json(devices);
});

app.get('/devices/:deviceId', (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    const { deviceId } = req.params;

    const device = mockDevices.find((dev) => dev.id === +deviceId);

    return res.json(device);
});

export { app };
