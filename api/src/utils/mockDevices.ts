import Status from '../types/Status';

export default [
    {
        id: 1,
        name: 'Device 1',
        vendor: 'Auchan Oradea',
        status: Status.UNKNOWN,
    },
    {
        id: 2,
        name: 'Device 2',
        vendor: 'Carrefour Episcopia',
        status: Status.DEGRADED,
    },
    {
        id: 3,
        name: 'Device 3',
        vendor: 'Auchan Cluj',
        status: Status.OK,
    },
    {
        id: 4,
        name: 'Device 4',
        vendor: 'Auchan Sibiu',
        status: Status.INACTIVE,
    },
    {
        id: 5,
        name: 'Device 5',
        vendor: 'Carrefour Sibiu',
        status: Status.UNKNOWN,
    },
    {
        id: 6,
        name: 'Device 6',
        vendor: 'Iulius Mall Cluj',
        status: Status.OK,
    },
    {
        id: 7,
        name: 'Device 7',
        vendor: 'Iulius Mall Suceava',
        status: Status.DEGRADED,
    },
    {
        id: 8,
        name: 'Device 8',
        vendor: 'Lotus Mall Oradea',
        status: Status.INACTIVE,
    },
    {
        id: 9,
        name: 'Device 9',
        vendor: 'Oradea Shopping City',
        status: Status.OK,
    },
    {
        id: 10,
        name: 'Device 10',
        vendor: 'Profi Oradea',
        status: Status.DEGRADED,
    },
    {
        id: 11,
        name: 'Device 11',
        vendor: 'Profi Cluj',
        status: Status.OK,
    },
    {
        id: 12,
        name: 'Device 12',
        vendor: 'Profi Brasov',
        status: Status.INACTIVE,
    },
    {
        id: 13,
        name: 'Device 13',
        vendor: 'Profi Sibiu',
        status: Status.CRITICAL,
    },
    {
        id: 14,
        name: 'Device 14',
        vendor: 'Profi Suceava',
        status: Status.CRITICAL,
    },
    {
        id: 15,
        name: 'Device 15',
        vendor: 'Profi Iasi',
        status: Status.OK,
    },
];
