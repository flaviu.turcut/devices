export default (deviceId: number) => fetch(`http://localhost:8080/devices/${deviceId}`).then((res) => res.json());
