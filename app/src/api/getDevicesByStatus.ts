import { Status } from '../types/Status';

export default (status: Status) => fetch(`http://localhost:8080/devices/status/${status}`).then((res) => res.json());
