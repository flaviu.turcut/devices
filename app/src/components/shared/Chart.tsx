import React, { ReactElement } from 'react';
import styled from 'styled-components';
import ColoredStatusColumn from './ColoredStatusColumn';
import { Color } from '../../types/Color';

type ChartItem = {
    color: Color;
    status: string;
    nrOfItems: number;
};

type ChartProps = {
    items: ChartItem[];
    onClick: Function;
};

const ChartColumn = styled.div<{
    color: Color;
    width: number;
}>`
    background: ${({ color }) => color};
    cursor: pointer;
    height: 100%;
    width: ${({ width }) => `${width}%`};
`;

const StatusesContainer = styled.div`
    align-items: center;
    display: grid;
    grid-template-columns: 4fr 1fr;
    margin: 20px 0;
`;

const StatusesRow = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
`;

const DevicesLength = styled.span`
    font-size: 30px;
`;

const ChartContainer = styled.div`
    display: flex;
    height: 30px;
    width: 100%;
`;

const StatusColumnContainer = styled.div`
    padding: 5px 0;
`;

function Chart({ items, onClick }: ChartProps) {
    function getChartContent() {
        const statusColumns: ReactElement[] = [];
        const chartColumns: ReactElement[] = [];
        const totalNrOfItems = items.reduce((acc, { nrOfItems }) => nrOfItems + acc, 0);

        items.forEach((item) => {
            const { status, color, nrOfItems } = item;
            const width = (nrOfItems / totalNrOfItems) * 100;

            statusColumns.push(
                <StatusColumnContainer key={`status-row-${status}`}>
                    <ColoredStatusColumn
                        cursor="pointer"
                        color={color}
                        onClick={() => onClick(status)}
                        status={`${nrOfItems} ${status}`}
                    />
                </StatusColumnContainer>,
            );

            chartColumns.push(
                <ChartColumn
                    color={color}
                    onClick={() => onClick(status)}
                    key={`status-column-${status}`}
                    width={width}
                />,
            );
        });

        return (
            <>
                <StatusesContainer>
                    <StatusesRow>{statusColumns}</StatusesRow>
                    <DevicesLength>{totalNrOfItems}</DevicesLength>
                </StatusesContainer>
                <ChartContainer>{chartColumns}</ChartContainer>
            </>
        );
    }

    return <>{getChartContent()}</>;
}

export default Chart;
