import React from 'react';
import { Color } from '../../types/Color';
import styled from 'styled-components';

type ColoredStatusColumnProps = {
    color: Color;
    status: string;
    cursor?: string;
    onClick?: (event: React.MouseEvent<HTMLSpanElement>) => void;
};

const StatusSpan = styled.span<{ color: Color; cursor: string }>`
    color: ${({ color }) => color};
    cursor: ${({ cursor }) => cursor};
    text-transform: capitalize;
`;

function ColoredStatusColumn({ color, status, cursor = '', onClick = () => {} }: ColoredStatusColumnProps) {
    return (
        <StatusSpan cursor={cursor} color={color} onClick={onClick}>
            {status}
        </StatusSpan>
    );
}

export default ColoredStatusColumn;
