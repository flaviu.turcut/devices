import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';
import Device from '../types/Device';
import { Color } from '../types/Color';
import getDevicesByStatus from '../api/getDevicesByStatus';
import ColoredStatusColumn from './shared/ColoredStatusColumn';
import { QueryParams } from '../types/QueryParams';

const Table = styled.table`
    border-collapse: collapse;
    border-radius: 10px;
    width: 100%;

    th,
    td {
        padding: 20px 0;
        text-align: start;
    }
    thead {
        background: #1e69b8;
        color: white;
    }

    tbody {
        tr {
            cursor: pointer;
            :nth-child(odd) {
                background: #f5f5f5;
            }
        }
    }
`;

function Status() {
    const history = useHistory();
    const [devices, setDevices] = useState<Device[]>([]);
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const { status } = useParams<QueryParams>();

    useEffect(() => {
        async function fetchDevices() {
            await getDevicesByStatus(status).then(
                (devices) => {
                    setDevices(devices);
                    setIsLoaded(true);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                },
            );
        }

        if (!isLoaded) {
            fetchDevices();
        }
    });

    if (error) {
        return <div>Error loading devices!</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else if (!devices.length) {
        return <div>No devices with this status found!</div>;
    }

    function getTableContent() {
        return devices.map((device) => (
            <tr key={`th-${device.id}`} onClick={() => history.push(`/devices/${device.id}`)}>
                <td>{device.id}</td>
                <td>{device.name}</td>
                <td>{device.vendor}</td>
                <td>
                    <ColoredStatusColumn
                        cursor="pointer"
                        color={Color[device.status.toUpperCase()]}
                        status={device.status}
                    />
                </td>
            </tr>
        ));
    }

    return (
        <div>
            <Table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Device Name</th>
                        <th>Vendor</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>{getTableContent()}</tbody>
            </Table>
        </div>
    );
}

export default Status;
