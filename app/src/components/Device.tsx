import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import DeviceType from '../types/Device';
import getDevice from '../api/getDevice';
import { QueryParams } from '../types/QueryParams';
import ColoredStatusColumn from './shared/ColoredStatusColumn';
import { Color } from '../types/Color';

const Title = styled.h1`
    text-align: center;
`;

const InfoRow = styled.div`
    display: grid;
    grid-template-columns: minmax(100px, 1fr) 5fr;

    div {
        background: #f5f5f5;
        border-bottom: 1px solid #80808042;
        padding: 20px;
    }

    div:nth-child(1) {
        background: #1e69b8;
        color: white;
    }
`;

function Device() {
    const { deviceId } = useParams<QueryParams>();
    const [device, setDevice] = useState<DeviceType | null>(null);
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
        async function fetchDevices() {
            await getDevice(+deviceId).then(
                (device) => {
                    setDevice(device);
                    setIsLoaded(true);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                },
            );
        }

        if (!isLoaded) {
            fetchDevices();
        }
    });

    if (error) {
        return <div>Error fetching the device!</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else if (!device) {
        return <div>No device with this id found!</div>;
    }

    return (
        <>
            <Title>Device Info Page</Title>
            <InfoRow>
                <div>ID</div>
                <div>{device.id}</div>
            </InfoRow>
            <InfoRow>
                <div>Device Name</div>
                <div>{device.name}</div>
            </InfoRow>
            <InfoRow>
                <div>Vendor</div>
                <div>{device.vendor}</div>
            </InfoRow>
            <InfoRow>
                <div>Status</div>
                <div>
                    <ColoredStatusColumn color={Color[device.status.toUpperCase()]} status={device.status} />
                </div>
            </InfoRow>
        </>
    );
}

export default Device;
