import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import { Status } from '../types/Status';
import getDevices from '../api/getDevices';
import Device from '../types/Device';
import Chart from './shared/Chart';
import formatStatusesForChart from '../utils/formatStatusesForChart';

const Title = styled.span`
    margin-bottom: 50px;
    font-size: 25px;
`;

function Devices() {
    const history = useHistory();
    const [devices, setDevices] = useState<Device[]>([]);
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);

    const handleDevicesRequest = (devices: Device[]) => {
        setDevices(devices);
        setIsLoaded(true);
    };

    useEffect(() => {
        async function fetchDevices() {
            await getDevices().then(
                (devices) => handleDevicesRequest(devices),
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                },
            );
        }

        if (!isLoaded) {
            fetchDevices();
        }
    });

    function handleClick(status: Status) {
        history.push(`/status/${status}`);
    }

    if (error) {
        return <div>Error loading devices</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else if (!devices) {
        return <div>No devices found!</div>;
    }

    return (
        <>
            <Title>Overview</Title>
            <Chart onClick={handleClick} items={formatStatusesForChart(devices)} />
        </>
    );
}

export default Devices;
