import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const Container = styled.div`
    text-align: center;
`;

function Home() {
    return (
        <Container>
            <h1>Welcome to the device device monitoring app!</h1>
            <h3>
                <Link to="/devices">Go to overview page</Link>
            </h3>
        </Container>
    );
}

export default Home;
