import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Devices from './components/Devices';
import Status from './components/Status';
import Device from './components/Device';
import Home from './components/Home';

function App() {
    return (
        <div className="App">
            <Router>
                <div>
                    <nav style={{ textAlign: 'center', marginBottom: '50px' }}>
                        <Link to="/">Home</Link>
                    </nav>

                    <Switch>
                        <Route exact path="/">
                            <Home />
                        </Route>
                        <Route exact path="/devices">
                            <Devices />
                        </Route>
                        <Route path="/status/:status">
                            <Status />
                        </Route>
                        <Route path="/devices/:deviceId">
                            <Device />
                        </Route>
                    </Switch>
                </div>
            </Router>
        </div>
    );
}

export default App;
