import { Status } from './Status';

type Device = {
    id: number;
    name: string;
    vendor: string;
    status: Status;
};

export default Device;
