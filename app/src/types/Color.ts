export enum Color {
    CRITICAL = '#FE004F',
    DEGRADED = '#F5A624',
    OK = '#A9E272',
    INACTIVE = '#000',
    UNKNOWN = '#4A4D4E',
}
