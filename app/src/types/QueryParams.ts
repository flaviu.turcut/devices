import { Status } from './Status';

export type QueryParams = {
    deviceId: string;
    status: Status;
};
