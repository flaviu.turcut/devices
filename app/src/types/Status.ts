export enum Status {
    CRITICAL = 'critical',
    DEGRADED = 'degraded',
    OK = 'ok',
    INACTIVE = 'inactive',
    UNKNOWN = 'unknown',
}
