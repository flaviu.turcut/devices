import { Status } from '../types/Status';
import { Color } from '../types/Color';
import Device from '../types/Device';

export default (devices: Device[]) => {
    const formattedStatuses = [
        {
            nrOfItems: 0,
            status: Status.CRITICAL,
            color: Color.CRITICAL,
        },
        {
            nrOfItems: 0,
            status: Status.OK,
            color: Color.OK,
        },
        {
            nrOfItems: 0,
            status: Status.DEGRADED,
            color: Color.DEGRADED,
        },
        {
            nrOfItems: 0,
            status: Status.UNKNOWN,
            color: Color.UNKNOWN,
        },
        {
            nrOfItems: 0,
            status: Status.INACTIVE,
            color: Color.INACTIVE,
        },
    ];

    devices.forEach((device: Device) => {
        const statusIndex = formattedStatuses.findIndex((item) => item.status === device.status);
        formattedStatuses[statusIndex].nrOfItems = formattedStatuses[statusIndex].nrOfItems + 1;
    });

    return formattedStatuses;
};
